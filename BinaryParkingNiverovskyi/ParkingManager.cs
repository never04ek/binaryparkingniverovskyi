using System;

namespace BinaryParkingNiverovskyi
{
    public static class ParkingManager
    {
        private static Parking _parking;

        public static void Start()
        {
            _parking = Parking.GetInstance();
            Console.WriteLine("Hello! I'm ParkingManager3000. I'll help to manage parking.\n" +
                              "Do you want to configure the program? (Press Y/y if you want, else the default settings will be left)");
            var ans = '\0';
            try
            {
                ans = Convert.ToChar(Console.ReadLine());
            }
            catch (FormatException)
            {
            }

            if (ans == 'Y' || ans == 'y')
                Settings();
            Menu();
        }

        private static void Settings()
        {
            Console.WriteLine(
                "1. Enter initial Parking Balance.");
            _parking.Balance = -1;
            do
            {
                try
                {
                    _parking.Balance = Convert.ToDouble(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Input string was not in a correct format.");
                }
            } while (_parking.Balance < 0);


            Console.WriteLine("2. Enter maximum Parking Capacity");
            _parking.MaxCapacity = 0;
            do
            {
                try
                {
                    _parking.MaxCapacity = Convert.ToUInt32(Console.ReadLine());
                }
                catch (Exception)
                {
                    Console.WriteLine("Input string was not in a correct format. Need an integer greater than zero ");
                }
            } while (_parking.MaxCapacity <= 0);

            Console.WriteLine(
                "3. Enter the frequency (in seconds) with which the vehicles money will decrease by N-time parking");
            _parking.PaymentPeriod = 0;
            do
            {
                try
                {
                    _parking.PaymentPeriod = Convert.ToUInt32(Console.ReadLine());
                }
                catch (Exception)
                {
                    Console.WriteLine("Input string was not in a correct format. Need an integer greater than zero ");
                }
            } while (_parking.PaymentPeriod <= 0);

            Console.WriteLine("4. Enter tariffs determining how much money Vehicles must pay for Parking N-time");
            _parking.Tariffs[Vehicle.Type.Bus] = -1;
            _parking.Tariffs[Vehicle.Type.PassengerCar] = -1;
            _parking.Tariffs[Vehicle.Type.Motorcycle] = -1;
            _parking.Tariffs[Vehicle.Type.Truck] = -1;
            do
            {
                try
                {
                    Console.WriteLine("Bus:");
                    _parking.Tariffs[Vehicle.Type.Bus] = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Passenger car:");
                    _parking.Tariffs[Vehicle.Type.PassengerCar] = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Truck:");
                    _parking.Tariffs[Vehicle.Type.Truck] = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Motorcycle:");
                    _parking.Tariffs[Vehicle.Type.Motorcycle] = Convert.ToDouble(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Input string was not in a correct format. Need a number greater than zero ");
                }
            } while (_parking.Tariffs[Vehicle.Type.Bus] < 0 || _parking.Tariffs[Vehicle.Type.Truck] < 0 ||
                     _parking.Tariffs[Vehicle.Type.Motorcycle] < 0 ||
                     _parking.Tariffs[Vehicle.Type.PassengerCar] < 0);

            Console.WriteLine(
                "5. Enter the coefficient of the fine, which will be charged from vehicles if it does not have enough money to pay for parking");
            _parking.PenaltyRatio = -1;
            do
            {
                try
                {
                    _parking.PenaltyRatio = Convert.ToDouble(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine(
                        "Input string was not in a correct format. Need a number greater than or equal to zero ");
                }
            } while (_parking.PenaltyRatio < 0);
        }


        private static void Menu()
        {
            while (true)
            {
                Console.WriteLine("What do you want to do?\n " +
                                  "1. - Find out the current Parking balance\n " +
                                  "2. - The amount of money earned in the last minute\n " +
                                  "3. – Find out the number of free / occupied parking places.\n " +
                                  "4. - Display all Parking Transactions for the last minute\n " +
                                  "5. – Print the entire history of transactions (reading data from the file Transactions.log).\n " +
                                  "6. - Display a list of all Vehicles.\n " +
                                  "7. - Add Vehicle to the Parking.\n " +
                                  "8. – Remove Vehicle from the Parking.\n " +
                                  "9. – Recharge the balance of a particular Vehicle.\n " +
                                  "0. – Exit the best application ParkingManager3000((( ");
                try
                {
                    switch (Convert.ToInt32(Console.ReadLine()))
                    {
                        case 1:
                            Console.WriteLine($"The Parking balance: {_parking.Balance}");
                            break;
                        case 2:
                            MinuteMoney();
                            break;
                        case 3:
                            ParkingPlaces();
                            break;
                        case 4:
                            Transaction.PrintMinuteTransactions();
                            break;
                        case 5:
                            PrintAllTransactions();
                            break;
                        case 6:
                            _parking.WriteAllVehicles();
                            break;
                        case 7:
                            AddVehicleToParking();
                            break;
                        case 8:
                            RemoveVehicleFromParking();
                            break;
                        case 9:
                            RechargeBalanceOfVehicle();
                            break;
                        case 0:
                            Console.WriteLine("It is very sad to say goodbye to you (((\nBut goodbye");
                            Environment.Exit(0);
                            break;
                        default:
                            Console.WriteLine("Input number was not correct.");
                            break;
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Input string was not in a correct format.");
                }
            }
        }

        private static void PrintAllTransactions()
        {
            Console.WriteLine(System.IO.File.ReadAllText(@"Transactions.log"));
        }


        private static void RechargeBalanceOfVehicle()
        {
            _parking.WriteAllVehicles();

            if (_parking.CurrentCapacity == 0) return;

            Console.WriteLine("Which vehicle?");
            var i = -1;
            do
            {
                try
                {
                    i = Convert.ToInt32(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Input string was not in a correct format. Need an integer greater than zero ");
                }
            } while (i > _parking._vehicles.Count || i <= 0);

            Console.WriteLine("Enter the amount of money to supplement: ");
            var amount = -1;
            while (amount < 0)
            {
                try
                {
                    amount = Convert.ToInt32(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Input string was not in a correct format. Need an integer greater than zero ");
                }
            }

            _parking._vehicles[i - 1].RechargeBalance(amount);
        }

        private static void RemoveVehicleFromParking()
        {
            Console.WriteLine("Which vehicle?");
            _parking.WriteAllVehicles();
            var i = -1;
            do
            {
                try
                {
                    i = Convert.ToInt32(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Input string was not in a correct format. Need an integer greater than zero ");
                }
            } while (i > _parking._vehicles.Count || i <= 0);

            Console.WriteLine(_parking.Remove(_parking._vehicles[i - 1])
                ? "Removed successfully"
                : "Vehicle is not removed");
        }

        private static void AddVehicleToParking()
        {
            Console.WriteLine("Enter type's number of vehicle:\nCar: 0, Bus: 1, Motorcycle: 2, Truck: 3");
            var type = -1;
            while (!(type == 0 || type == 1 || type == 2 || type == 3))
            {
                try
                {
                    type = Convert.ToInt32(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Input string was not in a correct format. Need integer");
                }
            }

            Console.WriteLine("Enter car number: ");
            var plate = Console.ReadLine();
            Console.WriteLine("Enter model of car: ");
            var model = Console.ReadLine();
            Console.WriteLine("Enter balance of car: ");
            var balance = -1;
            while (balance <= 0)
            {
                try
                {
                    balance = Convert.ToInt32(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Input string was not in a correct format. Need integer");
                }
            }

            _parking.Add(new Vehicle((Vehicle.Type) type, plate, model, balance));
        }

        private static void ParkingPlaces()
        {
            Console.WriteLine($"Free places: {_parking.FreePlaces()}, Occupied places: {_parking.CurrentCapacity}");
        }

        private static void MinuteMoney()
        {
            Console.WriteLine($"The amount of money earned in the last minute: {Transaction.MinuteSumOfIncome()}");
        }
    }
}