using System;
using System.Collections.Generic;

namespace BinaryParkingNiverovskyi
{
    public class Transaction
    {
        private String _uniqueId;
        private readonly DateTime _dateTime;
        private readonly double _tariff;
        private static List<Transaction> MinuteTransactions = new List<Transaction>();

        public Transaction(string uniqueID, double tariff)
        {
            _uniqueId = uniqueID;
            _dateTime = DateTime.Now;
            MinuteTransactions.Add(this);
            _tariff = tariff;
            AddToFile();
        }

        private void AddToFile()
        {
            using (var file =
                new System.IO.StreamWriter(@"Transactions.log", true))
            {
                file.WriteLine(this);
            }
        }

        private static void UpdateMinuteTransactions()
        {
            if (MinuteTransactions.Count == 0) return;
            while ((DateTime.Now - MinuteTransactions[0]._dateTime).TotalMinutes > 1)
            {
                MinuteTransactions.Remove(MinuteTransactions[0]);
            }
        }

        public static void PrintMinuteTransactions()
        {
            UpdateMinuteTransactions();
            Console.WriteLine($"Amount of transactions: {MinuteTransactions.Count}");
            for (var i = 0; i < MinuteTransactions.Count; i++)
            {
                Console.WriteLine(MinuteTransactions[i]);
            }
        }

        public static double MinuteSumOfIncome()
        {
            UpdateMinuteTransactions();
            var sum = 0.0;
            foreach (var transaction in MinuteTransactions)
            {
                sum += transaction._tariff;
            }

            return sum;
        }

        public override string ToString()
        {
            return $"{_dateTime} - UniqueID:{_uniqueId} - Tariff: {_tariff}";
        }
    }
}