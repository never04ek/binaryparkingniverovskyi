using System;

namespace BinaryParkingNiverovskyi
{
    public class Vehicle
    {
        public enum Type : int
        {
            PassengerCar = 0,
            Bus,
            Motorcycle,
            Truck
        }

        public bool IsParked { get; set; }
        public string Plate { get; }
        public double Balance { get; set; }
        public DateTime LastPaidTime { get; set; }
        public string Model { get; }

        public readonly Type TypeOfVehicle;

        public Vehicle(Type typeOfVehicle, string plate, string model, int balance)
        {
            Plate = plate;
            Model = model;
            Balance = balance;
            IsParked = false;
            TypeOfVehicle = typeOfVehicle;
            LastPaidTime = DateTime.Now;
        }

        public void RechargeBalance(int sum)
        {
            Balance += sum;
        }
    }
}